import java.util.Scanner;

class RasCalc extends ProstCalc {
    public RasCalc() {
        super (Num_1, Num_2, Add, Div, Mul, Sub);
    }

    Scanner input = new Scanner(System.in);

    public double getMnoshiteli (double mn){
        System.out.println("Введите число: ");
        int x = input.nextInt();
        double sqrt = Math.sqrt(x);
        int currentValue = x;
        int multiplier = 2;
        while (currentValue > 1 && multiplier <= sqrt)
        {
            if (currentValue % multiplier == 0)
            {
                System.out.print(multiplier + " ");
                currentValue /= multiplier;
            }
            else if (multiplier == 2)
            {
                multiplier++;
            }
            else
            {
                multiplier += 2;
            }
        }
        if (currentValue != 1)
        {
            System.out.print(currentValue + "\n");
        }
        return currentValue;
    }
}